git pull
docker build -t musers-back -f docker/gunicorn.Dockerfile .
docker build -t musers-static -f docker/static.Dockerfile .
docker stop musers-back
docker stop musers-static
docker run -d --network=host --rm --name musers-back musers-back
docker run -d -p 8012:80 --rm --name musers-static musers-static
docker run -d --network host --name musers-celery-beat --entrypoint='/bin/bash' musers-back -c 'celery beat -A musers.application -l error'
docker run -d --network host --name musers-celery-worker --entrypoint='/bin/bash' musers-back -c 'celery worker -A musers.application -l error'
docker exec musers-back python manage.py migrate

