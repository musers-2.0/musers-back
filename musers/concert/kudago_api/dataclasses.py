from dataclasses import field
from datetime import datetime
from typing import Optional, List

from django.utils.timezone import get_default_timezone
from marshmallow import pre_load, EXCLUDE
from marshmallow_dataclass import dataclass

from musers.concert.models import Concert, Place
from musers.utils.schemas import BaseDataclassSchema

LOCATIONS = {
    "ekb": "Екатеринбург",
    "krasnoyarsk": "Красноярск",
    "krd": "Краснодар",
    "kzn": "Казань",
    "mns": "Минск",
    "msk": "Москва",
    "nnv": "Нижний Новгород",
    "nsk": "Новосибирск",
    "online": "Онлайн",
    "sochi": "Сочи",
    "spb": "Санкт-Петербург",
}


@dataclass
class ApiPlace(BaseDataclassSchema):
    @pre_load
    def prepare(self, data: dict, **_):
        if data.get("coords"):
            data["latitude"] = data["coords"].get("lat")
            data["longitude"] = data["coords"].get("lon")

        if data.get("location"):
            data["location"] = LOCATIONS.get(data["location"])

        return data

    id: int = field()
    title: Optional[str] = field()
    address: Optional[str] = field()
    subway: Optional[str] = field()
    location: Optional[str] = field()
    latitude: Optional[float] = field()
    longitude: Optional[float] = field()

    @property
    def defaults(self) -> dict:
        return {
            "title": self.title,
            "address": self.address,
            "subway": self.subway,
            "location": self.location,
            "latitude": self.latitude,
            "longitude": self.longitude,
        }


@dataclass
class ApiConcert(BaseDataclassSchema):
    @pre_load
    def prepare(self, data: dict, **_):
        if data.get("dates"):
            data["start_date"] = str(
                datetime.fromtimestamp(
                    data["dates"][0]["start"], tz=get_default_timezone()
                )
                if data["dates"][0].get("start")
                else None
            )
        if data.get("location"):
            data["location"] = LOCATIONS.get(data["location"]["slug"])

        if data.get("images"):
            data["image_url"] = data["images"][0]["image"]

        if data.get("description"):
            data["short_description"] = data["description"]

        if data.get("body_full"):
            data["description"] = data["body_full"]
        return data

    id: int = field()
    start_date: Optional[datetime] = field()
    title: str = field()
    short_title: str = field()
    short_description: str = field()
    description: str = field()
    price: str = field()
    place: Optional[ApiPlace] = field()
    image_url: str = field()

    @property
    def model(self):
        return Concert(
            kudago_id=self.id,
            start_date=self.start_date,
            title=self.title,
            short_title=self.short_title,
            short_description=self.short_description,
            description=self.description,
            price=self.price,
            image_url=self.image_url,
        )


@dataclass
class ApiConcerts(BaseDataclassSchema):
    class Meta:
        unknown = EXCLUDE

    results: List[ApiConcert] = field()
