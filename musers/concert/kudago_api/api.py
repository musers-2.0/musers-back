import time
from logging import getLogger
from typing import Optional, List
from urllib.parse import urljoin
from fuzzywuzzy import fuzz

import requests

from musers.application import settings
from musers.artist.models import Artist
from musers.concert.kudago_api.dataclasses import ApiConcert, ApiConcerts
from musers.concert.models import Concert, Place, ConcertArtist


class KudagoApi:
    def __init__(self):
        self.logger = getLogger("kudago_api")

    def list_active_concerts(self) -> List[ApiConcert]:
        fetched = 0
        page = 1
        total = None
        concerts: List[ApiConcert] = []
        while total is None or fetched < total:
            data = self.execute(
                "events",
                params={
                    "fields": "id,dates,title,short_title,description,dates,place,location,price,images",
                    "text_format": "text",
                    "actual_since": time.time(),
                    "categories": "concert",
                    "page_size": 100,
                    "page": page,
                    "expand": "place",
                },
            )
            page += 1
            fetched += 100
            if total is None:
                total = data["count"]
            concerts += ApiConcerts.Schema().load(data).results

        concert_ids = {concert.id for concert in concerts}
        existed_concerts_ids = set(Concert.objects.values_list("kudago_id", flat=True))
        new_concerts_ids = concert_ids.difference(existed_concerts_ids)
        self.logger.info(f"Fetched {len(new_concerts_ids)} new concerts")
        concerts = list(filter(lambda x: x.id in new_concerts_ids, concerts))

        concert_ops = []
        for concert in concerts:
            concert_model = concert.model
            if concert.place:
                place, _ = Place.objects.get_or_create(
                    kudago_id=concert.place.id, defaults=concert.place.defaults
                )
                concert_model.place_id = place.id
            concert_ops.append(concert_model)

        Concert.objects.bulk_create(concert_ops)

        new_concerts = Concert.objects.filter(kudago_id__in=new_concerts_ids)
        artists = Artist.objects.all()
        concert_artists = []
        start_time = time.time()
        for concert in new_concerts:
            for artist in artists:
                probability = fuzz.ratio(
                    concert.short_title.lower(), artist.name.lower()
                )
                if probability > 80:
                    concert_artists.append(
                        ConcertArtist(
                            concert=concert,
                            artist=artist,
                            probability=probability,
                        )
                    )
        self.logger.info(
            f"Тегирование концертов заняло {time.time() - start_time} секунд"
        )
        ConcertArtist.objects.bulk_create(concert_artists)

    def execute(self, path: str, params: dict) -> Optional[dict]:
        url = urljoin(settings.KUDAGO_URL, path)
        r = requests.get(url, params=params)
        if r.status_code != 200:
            self.logger.error(
                f"request to {url} with params={params} failed with {r.status_code} status_code: {r.text}"
            )
            return None
        return r.json()
