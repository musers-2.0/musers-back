from django.contrib import admin

from musers.concert.models import Concert, Tag, Place, ConcertUser, ConcertArtist


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_display_links = list_display


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    list_display = ("title",)
    list_display_links = list_display


@admin.register(Concert)
class ConcertAdmin(admin.ModelAdmin):
    list_display = ("id", "title")
    list_display_links = list_display
    filter_horizontal = ("tags",)


@admin.register(ConcertUser)
class ConcertUserAdmin(admin.ModelAdmin):
    def get_user(self, obj):
        return obj.user.author

    list_display = ("id", "user", "concert", "is_going")
    list_filter = ("is_going",)
    list_display_links = list_display


@admin.register(ConcertArtist)
class ConcertArtistAdmin(admin.ModelAdmin):
    list_display = ("id", "concert", "artist", "probability")
    list_display_links = list_display
