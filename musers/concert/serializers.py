from django_serializer.v2.serializer import ModelSerializer, Serializer
from marshmallow import fields, pre_dump

from musers.concert.models import Concert, ConcertUser, Place
from musers.core.serializers import UserSerializer, LightUserSerializer


class PlaceSerializer(ModelSerializer):
    class SMeta:
        model = Place


class ConcertSerializer(ModelSerializer):
    class SMeta:
        exclude = ("place_id",)
        model = Concert

    place = fields.Nested(PlaceSerializer)


class ConcertSerializerWithUsers(ConcertSerializer):
    @pre_dump
    def prepare(self, obj: Concert, **_):
        concert_users = obj.concertuser_set.all()
        obj._users = [item.user for item in concert_users[:6]]
        return obj

    users_count = fields.Int(attribute="concert_users_count")
    is_going = fields.Boolean(default=False)
    users = fields.Nested(LightUserSerializer, many=True, attribute="_users")


class ConcertListSerializer(Serializer):
    count = fields.Int()
    items = fields.Nested(ConcertSerializerWithUsers, many=True)


class ConcertUserSerializer(ModelSerializer):
    class SMeta:
        model = ConcertUser


class ConcertUserListSerializer(Serializer):
    count = fields.Int()
    items = fields.Nested(UserSerializer, many=True)
