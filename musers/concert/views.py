from django.contrib.postgres.search import SearchVector
from django.db.models import Prefetch, QuerySet, Count, Q
from django.utils import timezone
from django_serializer.v2.views import ListApiView, ApiView, HttpMethod, GetApiView
from django_serializer.v2.views.mixins import LoginRequiredMixin

from musers.application.base import LimitOffsetPaginator
from musers.concert.forms import (
    ConcertListForm,
    ConcertUserToggleForm,
    ConcertUserListForm,
    DateFilterTypes,
)
from musers.concert.kudago_api.api import KudagoApi
from musers.concert.models import Concert, ConcertUser
from musers.concert.serializers import (
    ConcertListSerializer,
    ConcertUserSerializer,
    ConcertUserListSerializer,
    ConcertSerializerWithUsers,
)
from musers.core.models import User


class ConcertListView(ListApiView):
    class Meta:
        tags = ["concert"]
        model = Concert
        query_form = ConcertListForm
        serializer = ConcertListSerializer
        serializer_many = False
        paginator = LimitOffsetPaginator

    def filter_date(self, qs: QuerySet) -> QuerySet:
        date_filter = self.request_query.get("date_filter")
        if date_filter:
            if date_filter == DateFilterTypes.PASSED[0]:
                qs = qs.filter(start_date__lte=timezone.now())
            elif date_filter == DateFilterTypes.COMING[0]:
                qs = qs.filter(start_date__gt=timezone.now())
        return qs

    def filter_query(self, qs: QuerySet) -> QuerySet:
        query = self.request_query.get("query")
        if query:
            qs = qs.annotate(search=SearchVector("title")).filter(
                search__icontains=query
            )
        return qs

    def filter_by_user(self, qs: QuerySet) -> QuerySet:
        user_id = self.request_query.get("user_id")
        if user_id:
            qs = qs.filter(concertuser__user_id=user_id, concertuser__is_going=True)
        return qs

    def get_queryset(self):
        qs = super().get_queryset()
        qs = self.filter_query(qs)
        qs = self.filter_date(qs)
        qs = self.filter_by_user(qs)

        qs = qs.prefetch_related(
            Prefetch(
                "concertuser_set",
                queryset=ConcertUser.objects.select_related("user").filter(
                    is_going=True,
                ),
            )
        )

        qs = qs.annotate(
            concert_users_count=Count("concertuser", Q(concertuser__is_going=True))
        )
        qs = qs.order_by("-concert_users_count")

        if self.request.user.is_authenticated:
            user_concert_ids = set(
                ConcertUser.objects.select_related("user")
                .filter(user_id=self.request.user.id, is_going=True)
                .values_list("concert_id", flat=True)
            )

            concerts, mine = [], self.request_query.get("mine")
            for concert in qs:
                if concert.id in user_concert_ids:
                    concert.is_going = True
                    if mine:
                        concerts.append(concert)
                        continue
                concerts.append(concert)
        else:
            concerts = qs

        return concerts

    def build_response(self, qs, qs_after_paginator=None):
        return {"items": qs_after_paginator, "count": len(qs)}


class ConcertListRecommendedView(LoginRequiredMixin, ListApiView):
    class Meta:
        tags = ["concert"]
        model = Concert
        method = HttpMethod.GET
        serializer_many = False
        serializer = ConcertListSerializer
        paginator = LimitOffsetPaginator

    def get_queryset(self):
        artists = self.request.user.artists.values_list("artist_id", flat=True)
        qs = Concert.objects.all()
        qs = qs.prefetch_related(
            Prefetch(
                "concertuser_set",
                queryset=ConcertUser.objects.select_related("user").filter(
                    is_going=True,
                ),
            )
        )
        qs = qs.annotate(
            artists_count=Count("concertartist", filter=Q(concertartist__artist_id__in=artists))
        )
        qs = qs.annotate(
            concert_users_count=Count("concertuser", Q(concertuser__is_going=True))
        )
        qs = qs.order_by("-artists_count", "-concert_users_count")
        user_concert_ids = set(
            ConcertUser.objects.select_related("user")
            .filter(user_id=self.request.user.id, is_going=True)
            .values_list("concert_id", flat=True)
        )

        concerts = []
        for concert in qs:
            if concert.id in user_concert_ids:
                concert.is_going = True
            concerts.append(concert)
        return concerts

    def build_response(self, qs, qs_after_paginator=None):
        return {"items": qs_after_paginator, "count": len(qs)}


class ConcertUserToggleView(LoginRequiredMixin, ApiView):
    class Meta:
        tags = ["concert"]
        method = HttpMethod.POST
        body_form = ConcertUserToggleForm
        serializer = ConcertUserSerializer

    def execute(self, request, *args, **kwargs):
        concert_user, created = ConcertUser.objects.get_or_create(
            concert=self.request_body["concert_id"], user=self.request.user
        )

        if created:
            return concert_user

        concert_user.is_going = not concert_user.is_going
        concert_user.save(update_fields=["is_going"])

        return concert_user


class ConcertUserListView(ListApiView):
    class Meta:
        tags = ["concert"]
        model = User
        query_form = ConcertUserListForm
        serializer = ConcertUserListSerializer
        serializer_many = False
        paginator = LimitOffsetPaginator

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(concertuser__concert=self.request_query['concert_id'], concertuser__is_going=True)
        return qs.distinct()

    def build_response(self, qs, qs_after_paginator=None):
        return {"items": qs_after_paginator, "count": qs.count()}


class ConcertGetView(GetApiView):
    class Meta:
        tags = ["concert"]
        model = Concert
        method = HttpMethod.GET
        serializer = ConcertSerializerWithUsers

    def get_object(self) -> Concert:
        qs = Concert.objects.filter(id=self.request_query["id"])
        if self.request.user.is_authenticated:
            qs = qs.prefetch_related(
                Prefetch(
                    "concertuser_set",
                    queryset=ConcertUser.objects.select_related("user").filter(
                        is_going=True,
                    ),
                )
            )
            concert = qs.get()
            concert.is_going = ConcertUser.objects.filter(
                user_id=self.request.user.id, concert=concert, is_going=True
            )
            return concert
        return qs.get()


class FetchNewConcerts(ApiView):
    class Meta:
        tags = ["concerts"]
        method = HttpMethod.GET

    def execute(self, request, *args, **kwargs):
        KudagoApi().list_active_concerts()
