from django.db import models

from musers.artist.models import Artist
from musers.core.models import User


class Place(models.Model):
    class Meta:
        verbose_name = "место проведения"
        verbose_name_plural = "места проведения"

    kudago_id = models.IntegerField(verbose_name="KudaGo id")
    title = models.CharField(
        max_length=128, blank=True, null=True, verbose_name="название"
    )
    address = models.CharField(
        max_length=128, blank=True, null=True, verbose_name="адрес"
    )
    subway = models.CharField(
        max_length=128, blank=True, null=True, verbose_name="станция метро"
    )
    location = models.CharField(
        max_length=128, blank=True, null=True, verbose_name="локация"
    )
    latitude = models.FloatField(blank=True, null=True, verbose_name="широта")
    longitude = models.FloatField(blank=True, null=True, verbose_name="долгота")

    def __str__(self):
        if self.title:
            return self.title
        return "место без названия"


class Tag(models.Model):
    class Meta:
        verbose_name = "тэг"
        verbose_name_plural = "тэги"

    name = models.CharField(max_length=32, verbose_name="название")

    def __str__(self):
        return self.name


class ConcertUser(models.Model):
    class Meta:
        verbose_name = "пользователь концерта"
        verbose_name_plural = "пользователи концерта"
        unique_together = ("user", "concert")

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="пользователь"
    )
    concert = models.ForeignKey(
        "Concert", on_delete=models.CASCADE, verbose_name="концерт"
    )
    is_going = models.BooleanField(default=True, verbose_name="идет на концерт?")
    created = models.DateTimeField(auto_now_add=True, verbose_name="время отметки")


class ConcertArtist(models.Model):
    class Meta:
        verbose_name = "концерт артиста"
        verbose_name_plural = "концерты артиста"

    concert = models.ForeignKey(
        "Concert",
        on_delete=models.CASCADE,
        verbose_name="концерт",
    )
    artist = models.ForeignKey(
        Artist, related_name="concerts", on_delete=models.CASCADE, verbose_name="артист"
    )
    probability = models.FloatField(null=True, verbose_name="вероятность")


class Concert(models.Model):
    class Meta:
        verbose_name = "концерт"
        verbose_name_plural = "концерты"

    kudago_id = models.IntegerField(verbose_name="id kudago")
    title = models.CharField(max_length=128, verbose_name="заголовок")
    short_title = models.CharField(
        max_length=128, blank=True, null=True, verbose_name="Краткий заголовок"
    )
    short_description = models.CharField(
        max_length=512, verbose_name="краткое описание"
    )
    description = models.TextField(verbose_name="описание")
    price = models.CharField(verbose_name="цена", max_length=128, blank=True, null=True)
    start_date = models.DateTimeField(verbose_name="дата и время проведения")
    image_url = models.CharField(max_length=256, verbose_name="ссылка на картинку")

    users = models.ManyToManyField(
        User, through=ConcertUser, related_name="users", verbose_name="пользователи"
    )
    artists = models.ManyToManyField(
        Artist, through=ConcertArtist, related_name="artists", verbose_name="артисты"
    )

    place = models.ForeignKey(
        Place, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="место"
    )
    tags = models.ManyToManyField(Tag, verbose_name="тэги")

    def __str__(self):
        return self.title
