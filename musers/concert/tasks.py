from celery.schedules import crontab
from celery.task import periodic_task

from musers.concert.kudago_api.api import KudagoApi


@periodic_task(run_every=crontab(hour=5, minute=0))
def fetch_new_concerts():
    KudagoApi().list_active_concerts(),
