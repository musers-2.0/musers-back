# Generated by Django 3.0.4 on 2021-05-15 18:21

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("concert", "0004_auto_20210308_1828"),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name="concertuser",
            unique_together={("user", "concert")},
        ),
    ]
