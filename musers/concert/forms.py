from django import forms
from django.core.exceptions import ValidationError

from musers.concert.models import Concert
from musers.core.models import User


class DateFilterTypes:
    PASSED = ("passed", "прошедшие")
    COMING = ("coming", "будущие")


class ConcertListForm(forms.Form):
    query = forms.CharField(required=False)
    mine = forms.BooleanField(required=False)
    user_id = forms.ModelChoiceField(queryset=User.objects.all(), required=False)
    date_filter = forms.ChoiceField(
        choices=[DateFilterTypes.PASSED, DateFilterTypes.COMING], required=False
    )

    def clean(self):
        data = self.cleaned_data
        if data.get("mine") and data.get("user_id"):
            raise ValidationError("Нельзя одновременно передать user_id и mine")
        return data


class ConcertUserToggleForm(forms.Form):
    concert_id = forms.ModelChoiceField(queryset=Concert.objects.all())


class ConcertUserListForm(forms.Form):
    concert_id = forms.ModelChoiceField(queryset=Concert.objects.all())
