from django.urls import path

from musers.concert.views import (
    ConcertListView,
    ConcertUserToggleView,
    ConcertUserListView,
    ConcertGetView,
    FetchNewConcerts, ConcertListRecommendedView,
)

urlpatterns = [
    path("list", ConcertListView.as_view()),
    path("list_recommended", ConcertListRecommendedView.as_view()),
    path("toggle", ConcertUserToggleView.as_view()),
    path("list_users", ConcertUserListView.as_view()),
    path("get", ConcertGetView.as_view()),
    path("fetch_new", FetchNewConcerts.as_view()),
]
