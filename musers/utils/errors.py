from django_serializer.v2.exceptions import HttpError


class VkApiError(HttpError):
    def __init__(self):
        self.http_code = 503
        self.alias = "vk_api_error"
        self.description = "vk api error"
