from typing import ClassVar, Type

from marshmallow import EXCLUDE


class BaseDataclassSchema:
    from marshmallow import Schema

    Schema: ClassVar[Type[Schema]] = Schema  # noqa

    class Meta:
        unknown = EXCLUDE
