from django.db import models

from musers.core.models import User


class Artist(models.Model):
    class Meta:
        verbose_name = "артист"
        verbose_name_plural = "артисты"

    name = models.CharField(max_length=128, verbose_name="название")
    user = models.ManyToManyField(
        User, through="UserArtist", verbose_name="пользователи"
    )

    def __str__(self):
        return self.name


class UserArtist(models.Model):
    class Meta:
        verbose_name = "артист пользователя"
        verbose_name_plural = "артисты пользователя"

    user = models.ForeignKey(
        User,
        related_name="artists",
        on_delete=models.CASCADE,
        verbose_name="пользователь",
    )
    artist = models.ForeignKey(
        Artist, related_name="users", on_delete=models.CASCADE, verbose_name="артист"
    )
    count = models.PositiveIntegerField(default=1, verbose_name="число треков")
