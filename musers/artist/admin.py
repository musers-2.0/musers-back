from django.contrib import admin

from musers.artist.models import Artist, UserArtist


@admin.register(Artist)
class ArtistAdmin(admin.ModelAdmin):
    list_display = ("id", "name")
    list_display_links = list_display


@admin.register(UserArtist)
class UserArtistAdmin(admin.ModelAdmin):
    list_display = ("id", "user", "artist", "count")
    list_display_links = list_display
