import os
from configparser import ConfigParser

from django.contrib.postgres.fields import JSONField
from django.forms import TypedChoiceField, Field, ChoiceField, NullBooleanField
from marshmallow import fields

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

PLATFORM_CONFIG_NAME = "musers.conf"
production_config = os.path.join("/etc", "musers", PLATFORM_CONFIG_NAME)
development_config = os.path.join(BASE_DIR, "conf", PLATFORM_CONFIG_NAME)
config_path = (
    production_config if os.path.exists(production_config) else development_config
)
config = ConfigParser()

config.read(config_path)

SECRET_KEY = config.get("common", "secret_key", fallback="my-secret-key")
DEBUG = config.getboolean("common", "debug", fallback=False)
SILK = config.getboolean("common", "silk", fallback=True)

BASE_DOMAIN = config.get("common", "base_domain", fallback="https://musers.me")

ALLOWED_HOSTS = ["*"]

# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_celery_beat",
    "django_serializer",
]

PROJECT_APPS = [
    "musers.core",
    "musers.concert",
    "musers.artist",
]

INSTALLED_APPS += PROJECT_APPS

if SILK:
    INSTALLED_APPS += ["silk"]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

if SILK:
    MIDDLEWARE = ["silk.middleware.SilkyMiddleware"] + MIDDLEWARE

ROOT_URLCONF = "musers.application.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "musers", "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "musers.application.wsgi.application"

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": config.get("database", "name", fallback=""),
        "USER": config.get("database", "user", fallback=""),
        "PASSWORD": config.get("database", "password", fallback=""),
        "HOST": config.get("database", "host", fallback="127.0.0.1"),
        "PORT": config.get("database", "port", fallback="5432"),
        "TEST": {"CHARSET": "UTF8", "TEMPLATE": "template0"},
    },
}

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

AUTH_USER_MODEL = "core.User"

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = "ru-RU"
TIME_ZONE = "Europe/Moscow"

CELERY_TIMEZONE = "Europe/Moscow"

USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = "/dj_static/"
STATIC_ROOT = os.path.join(BASE_DIR, "static")

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "INFO",
    },
    "loggers": {
        "django": {
            "handlers": ["console"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "INFO"),
            "propagate": False,
        },
    },
}

# CELERY SETTINGS
CELERY_BROKER_URL = config.get(
    "celery", "broker_url", fallback="redis://localhost:6379/0"
)
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers.DatabaseScheduler"
CELERY_IMPORTS = ("musers.concert.tasks",)

SERIALIZER_FORM_FIELD_MAPPING = {
    TypedChoiceField: fields.Str,
    ChoiceField: fields.Str,
    Field: fields.Field,
    JSONField: fields.Raw,
    NullBooleanField: fields.Bool,
}

CLIENT_ID = config.get("oauth", "client_id", fallback="")
CLIENT_SECRET = config.get("oauth", "client_secret", fallback="")
REDIRECT_URI = config.get("oauth", "redirect_uri", fallback="")
VK_API_VERSION = config.get("vk", "api_version", fallback="5.130")
APP_TOKEN = config.get("vk", "app_token", fallback="")

AUDIO_MICROSERVICE_URL = config.get("microservice", "url", fallback=None)

KUDAGO_URL = config.get("kudago", "url", fallback="https://kudago.com/public-api/v1.4/")
