from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path("admin/", admin.site.urls),
    path("user/", include("musers.core.urls")),
    path("concert/", include("musers.concert.urls")),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.SILK:
    urlpatterns += [
        re_path(r"silk/", include("silk.urls", namespace="silk")),
    ]
