from django import forms
from django_serializer.v2.views.paginator import Paginator


class LimitOffsetPaginator(Paginator):
    class LimitOffsetForm(forms.Form):
        offset = forms.IntegerField(min_value=0, required=False)
        limit = forms.IntegerField(min_value=1, required=False)

    default_limit = 10
    condition_kwarg = None
    form = LimitOffsetForm

    def paginate(self, qs):
        offset = self.data["offset"] if self.data["offset"] is not None else 0
        limit = self.data["limit"] or self.default_limit
        return qs[offset : offset + limit]
