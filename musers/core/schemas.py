from marshmallow import Schema, fields


class VkOAuthAccessTokenResponse(Schema):
    access_token = fields.Str()
    expires = fields.Int()
    user_id = fields.Int()
