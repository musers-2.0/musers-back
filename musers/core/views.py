import requests
from django.conf import settings
from django.contrib.auth import login, logout
from django.db.models import Prefetch
from django.shortcuts import redirect
from django.utils import timezone
from django_serializer.v2.exceptions import (
    BadRequestError,
    NotFoundError,
)
from django_serializer.v2.views import ApiView, HttpMethod, ListApiView, GetApiView
from django_serializer.v2.views.mixins import LoginRequiredMixin
from marshmallow import ValidationError

from musers.application.base import LimitOffsetPaginator
from musers.application.settings import BASE_DOMAIN
from musers.artist.models import Artist, UserArtist
from musers.concert.models import ConcertUser
from musers.core.forms import OAuthVkForm, UserListRecommendationsForm
from musers.core.models import User
from musers.core.schemas import VkOAuthAccessTokenResponse
from musers.core.serializers import (
    UsersListSerializer,
    UserSerializer,
    UsersListRecommendedSerializer,
)


class UserCurrentView(LoginRequiredMixin, ApiView):
    class Meta:
        tags = ["user"]
        method = HttpMethod.GET
        serializer = UserSerializer

    def execute(self, request, *args, **kwargs):
        user = self.request.user
        user.concerts_count = ConcertUser.objects.filter(
            is_going=True,
            concert__start_date__gt=timezone.now(),
            user_id=self.request.user.id,
        ).count()
        return self.request.user


class OAuthVkView(ApiView):
    class Meta:
        tags = ["oauth"]
        method = HttpMethod.GET
        query_form = OAuthVkForm

    def execute(self, request, *args, **kwargs):
        if self.request_query.get("code"):
            resp = requests.get(
                f"https://oauth.vk.com/access_token?"
                f"client_id={settings.CLIENT_ID}&"
                f"client_secret={settings.CLIENT_SECRET}&"
                f"redirect_uri={settings.REDIRECT_URI}&"
                f'code={self.request_query["code"]}'
            )
            data = resp.json()
            if not resp.ok or not data:
                raise BadRequestError
            try:
                VkOAuthAccessTokenResponse().dump(data)
            except ValidationError:
                raise BadRequestError
            access_token = data["access_token"]
            user = User.objects.create_or_update_vk_user(access_token)
            login(request, user)

    def perform_response_pipelines(self, response):
        return redirect(to=BASE_DOMAIN)


class UserGetView(GetApiView):
    class Meta:
        tags = ["user"]
        model = User
        method = HttpMethod.GET
        serializer = UserSerializer

    def get_object(self):
        user = super().get_object()
        if user.is_superuser or user.is_staff:
            raise NotFoundError
        user.concerts_count = ConcertUser.objects.filter(
            is_going=True,
            concert__start_date__lt=timezone.now(),
            user_id=user.id,
        ).count()
        return user


class UsersListView(ListApiView):
    class Meta:
        tags = ["users"]
        model = User
        serializer = UsersListSerializer
        serializer_many = False
        paginator = LimitOffsetPaginator

    def get_queryset(self):
        return User.objects.filter(is_superuser=False, is_staff=False, is_active=True)

    def build_response(self, qs, qs_after_paginator=None):
        qs_after_paginator = qs_after_paginator.prefetch_related(
            Prefetch(
                "concertuser_set",
                queryset=ConcertUser.objects.filter(
                    is_going=True, concert__start_date__lt=timezone.now()
                ),
            )
        )
        for user in qs_after_paginator:
            user.concerts_count = len(user.concertuser_set.all())

        return {"items": qs_after_paginator, "count": qs.count()}


class UserListRecommendationsView(LoginRequiredMixin, ApiView):
    class Meta:
        tags = ["users"]
        method = HttpMethod.GET
        query_form = UserListRecommendationsForm
        serializer = UsersListRecommendedSerializer

    def execute(self, request, *args, **kwargs):
        limit = self.request_query["limit"]
        offset = self.request_query["offset"]
        try:
            response = requests.get(
                f"{settings.AUDIO_MICROSERVICE_URL}/"
                f"model.list_recommended?user_id={self.request.user.vk_id}"
                f"&limit={limit}&offset={offset}"
            )
            data = response.json()["data"]
            self.logger.info(f"response to list_recommended: {data}")

        except Exception as e:
            self.logger.exception("can not fetch recommended users", exc_info=e)
            return {
                "status": "processing",
                "items": [],
            }
        if response.status_code != 200:
            self.logger.exception(f"can not fetch recommended users: {response.text}")
            return {
                "status": "processing",
                "items": [],
            }

        users = User.objects.filter(vk_id__in=data["users"]).prefetch_related(
            Prefetch(
                "concertuser_set",
                queryset=ConcertUser.objects.filter(
                    is_going=True, concert__start_date__lt=timezone.now()
                ),
            )
        )
        for user in users:
            user.concerts_count = len(user.concertuser_set.all())
        return {
            "status": "ok",
            "count": data["count"],
            "items": users,
        }


class UserLogoutView(LoginRequiredMixin, ApiView):
    class Meta:
        tags = ["users"]
        method = HttpMethod.GET

    def execute(self, request, *args, **kwargs):
        logout(request)
        return {}


class AudioMicroserviceCallbackView(ApiView):
    class Meta:
        tags = ["users"]
        method = HttpMethod.POST

    def execute(self, request, *args, **kwargs):
        data = self.get_request_json()
        model = data["model"]
        data = data["data"]
        if model == "artists":
            vk_user_id = data["user_id"]
            try:
                user = User.objects.get(vk_id=vk_user_id)
            except User.DoesNotExist:
                return {}
            artists = data["artists"]
            grouped_artists = {}
            for artist in artists:
                try:
                    grouped_artists[artist] += 1
                except KeyError:
                    grouped_artists[artist] = 1

            ops = []
            for artist, tracks_count in grouped_artists.items():
                artist, _ = Artist.objects.get_or_create(name=artist)
                ops.append(
                    UserArtist(user_id=user.id, artist_id=artist.id, count=tracks_count)
                )

            UserArtist.objects.bulk_create(ops)
            user.audios_status = "processed"
            user.save(update_fields=["audios_status"])
        else:
            raise NotImplementedError
        return {}
