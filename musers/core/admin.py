from django.contrib import admin

from musers.core.models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "vk_id", "first_name", "last_name")
    list_display_links = list_display


def unregister_extra_models() -> None:
    from django_celery_beat.models import (
        CrontabSchedule,
        IntervalSchedule,
        PeriodicTask,
        SolarSchedule,
        ClockedSchedule,
    )

    admin.site.unregister(CrontabSchedule)
    admin.site.unregister(IntervalSchedule)
    admin.site.unregister(PeriodicTask)
    admin.site.unregister(SolarSchedule)
    admin.site.unregister(ClockedSchedule)


unregister_extra_models()
