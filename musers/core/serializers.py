from django_serializer.v2.serializer import Serializer, ModelSerializer
from marshmallow import fields

from musers.core.models import User


class UserSerializer(ModelSerializer):
    class SMeta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "avatar",
            "domain",
            "city",
            "audios_status",
        )

    age = fields.Int(allow_none=True)
    concerts_count = fields.Int(default=0)


class LightUserSerializer(ModelSerializer):
    class SMeta:
        model = User
        fields = (
            "id",
            "first_name",
            "avatar",
        )


class UsersListSerializer(Serializer):
    count = fields.Int()
    items = fields.Nested(UserSerializer, many=True)


class UsersListRecommendedSerializer(Serializer):
    status = fields.Str()
    count = fields.Int()
    items = fields.Nested(UserSerializer, many=True)
