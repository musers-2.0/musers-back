from django.urls import path

from musers.core.views import (
    OAuthVkView,
    UsersListView,
    UserGetView,
    UserCurrentView,
    AudioMicroserviceCallbackView,
    UserLogoutView,
    UserListRecommendationsView,
)

urlpatterns = [
    path("oauth", OAuthVkView.as_view()),
    path("get", UserGetView.as_view()),
    path("list", UsersListView.as_view()),
    path("current", UserCurrentView.as_view()),
    path("logout", UserLogoutView.as_view()),
    path("list_recommended", UserListRecommendationsView.as_view()),
    path("audio_callback", AudioMicroserviceCallbackView.as_view()),
]
