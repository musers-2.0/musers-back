from logging import getLogger
from typing import Optional
from urllib.parse import urlencode

import requests
from urllib3.util import Url

from musers.application import settings
from musers.core.vk_api.dataclasses import VkUser
from musers.utils.errors import VkApiError


class VkMethods:
    class Users:
        GET = "users.get"


vk_api_logger = getLogger("vk")


class VkApi:
    def __init__(self, access_token: str = None):
        self.access_token = access_token
        self.logger = vk_api_logger
        self.base_params = {"v": settings.VK_API_VERSION}

    def get_me(self) -> Optional[VkUser]:
        data = self.execute(
            method=VkMethods.Users.GET,
            params={"fields": "photo_max_orig,domain,bdate,city"},
        )
        user_data = data[0]
        if user_data.get("city"):
            user_data["city"] = user_data["city"]["title"]
        return VkUser.Schema().load(user_data)

    def execute(
        self, method: str, params: dict = None, use_app_key: bool = False
    ) -> dict:
        if params is None:
            params = {}

        params = {**self.base_params, **params}

        if use_app_key:
            params["token"] = settings.APP_TOKEN
        elif self.access_token:
            params["access_token"] = self.access_token

        request_url = Url(
            scheme="https",
            host=f"api.vk.com/method/{method}",
            query=urlencode(params),
        )

        resp = requests.get(str(request_url))
        if resp.status_code != 200:
            self.logger.error(
                f"VkApi execute() failed: "
                f"status {resp.status_code}, "
                f"resp: {resp.json()}"
            )
            raise VkApiError
        self.logger.info(
            f"VkApi execute(): request {request_url}, response: {resp.json()}"
        )

        return resp.json()["response"]
