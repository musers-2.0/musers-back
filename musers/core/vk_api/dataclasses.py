from dataclasses import field
from datetime import date
from typing import Optional

from marshmallow_dataclass import dataclass

from musers.utils.schemas import BaseDataclassSchema


@dataclass
class VkUser(BaseDataclassSchema):
    id: int = field()
    first_name: str = field()
    last_name: str = field()
    is_closed: bool = field()
    photo_max_orig: str = field()
    domain: str = field()
    birthday: Optional[date] = field(
        metadata={"data_key": "bdate", "format": "%d.%m.%Y"}
    )
    city: Optional[str] = field()
