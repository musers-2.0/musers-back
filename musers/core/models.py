from datetime import datetime
from typing import Optional

import requests
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models

from musers.core.vk_api.vk import VkApi


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, vk_id, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        user = self.model(vk_id=vk_id, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, vk_id, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(vk_id, password, **extra_fields)

    def create_or_update_vk_user(self, access_token: str):
        user = VkApi(access_token).get_me()
        response = requests.get(
            f"{settings.AUDIO_MICROSERVICE_URL}/vk.list_audios?owner_id={user.id}"
        )
        print(response.status_code)
        print(response.text)
        audios_status = response.json()["data"]["status"]
        user, _ = self.update_or_create(
            vk_id=user.id,
            defaults={
                "access_token": access_token,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "city": user.city,
                "is_closed": user.is_closed,
                "avatar": user.photo_max_orig,
                "domain": user.domain,
                "birthday": user.birthday,
                "audios_status": audios_status,
            },
        )
        return user


class User(AbstractBaseUser, PermissionsMixin):
    objects = UserManager()

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    first_name = models.CharField(verbose_name="Имя", max_length=30)
    last_name = models.CharField(verbose_name="Фамилия", max_length=30)
    is_closed = models.BooleanField(default=False, verbose_name="закрытый профиль?")
    avatar = models.CharField(
        verbose_name="аватар", max_length=256, blank=True, null=True
    )
    domain = models.CharField(verbose_name="текстовый id", max_length=128)
    city = models.CharField(verbose_name="город", max_length=128, blank=True, null=True)
    birthday = models.DateField(verbose_name="дата рождения", blank=True, null=True)

    vk_id = models.IntegerField(
        db_index=True, verbose_name="id пользователя vk", unique=True
    )
    access_token = models.CharField(
        max_length=128, verbose_name="токен пользователя vk", blank=True, null=True
    )
    audios_status = models.CharField(
        max_length=16,
        choices=[
            ("blocked", "заблокировано"),
            ("processing", "в обработке"),
            ("processed", "обработано"),
        ],
        default="blocked",
    )

    is_staff = models.BooleanField(default=False, verbose_name="персонал?")
    is_active = models.BooleanField(default=True, verbose_name="активный?")
    created = models.DateTimeField(auto_now_add=True, verbose_name="создан")

    USERNAME_FIELD = "vk_id"
    EMAIL_FIELD = "vk_id"

    def get_full_name(self):
        return f"{self.last_name} {self.first_name}"

    def __str__(self):
        return self.get_full_name()

    @property
    def age(self) -> Optional[int]:
        if not self.birthday:
            return None
        return relativedelta(datetime.now(), self.birthday).years
