from django import forms


class OAuthVkForm(forms.Form):
    code = forms.CharField(required=False)


class UserListRecommendationsForm(forms.Form):
    limit = forms.IntegerField()
    offset = forms.IntegerField()
